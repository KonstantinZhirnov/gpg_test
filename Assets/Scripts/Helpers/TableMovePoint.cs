﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableMovePoint : MonoBehaviour
{
    private void OnDrawGizmos ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere (transform.position, 10);

        Vector3 direction = transform.TransformDirection (Vector3.down) * 60;
        Gizmos.DrawRay (transform.position, direction);
    }
}
