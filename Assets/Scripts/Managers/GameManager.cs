﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    PizzaView _pizzaView;
    [SerializeField]
    RotateTable _table;

    public RotateTable Table { get { return _table; } }

    Pizza _pizza;
    User _currentUser;
    public User CurrentUser { get => _currentUser; set => _currentUser = value; }

    private void Start ()
    {
        CurrentUser = new User ();
        //StartCoroutine (Test ());

        _pizza = new Pizza (4, 5);
        _pizza.OnPiaceEaten += _pizza_OnPiaceEaten;
        _pizzaView.Init (_pizza);

    }

    private void _pizza_OnPiaceEaten (Pizza obj)
    {
        _currentUser.Level.CurrentXP += obj.PiecePrise;
    }

    IEnumerator Test () {
        yield return null;
        while(_pizza != null) {
            yield return new WaitForSeconds (3);
            _pizza.EatPiece ();
            
        }
    }
}
