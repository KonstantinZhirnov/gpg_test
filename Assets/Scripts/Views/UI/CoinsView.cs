﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CoinsView : MonoBehaviour
{
    [SerializeField]
    Text _value;
    [SerializeField]
    float _updateTime;

    float _currentValue;
    float _maxValue;

    DateTime _updateFinish;

    float _delta;
    bool isUpdateNeed;

    public void SetCoins(float coins)
    {
        _maxValue = coins;
        _updateFinish = DateTime.Now.AddSeconds (_updateTime);
        isUpdateNeed = true;
    }

    private void Start ()
    {
        GameManager.Instance.CurrentUser.OnCoinsChanged += CurrentUser_OnCoinsChanged;
        _updateFinish = DateTime.Now;
    }

    private void CurrentUser_OnCoinsChanged (float obj)
    {
        SetCoins (obj);
    }

    
    private void Update ()
    {
        if (!isUpdateNeed) return;

        if(_updateFinish > DateTime.Now) {
            TimeSpan leftToFinish = _updateFinish - DateTime.Now;
            _delta = (_maxValue - _currentValue)/(float)leftToFinish.TotalMilliseconds;
            _currentValue += _delta * Time.deltaTime * 1000;
            if (_currentValue > _maxValue) {
                _currentValue = _maxValue;
                isUpdateNeed = false;
            }
            _value.text = string.Format ("{0:0}", _currentValue);
        }  else {
            _currentValue = _maxValue;
            _value.text = string.Format ("{0:0}", _currentValue);
            isUpdateNeed = false;
        }
        
    }


}
