﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeaderView : MonoBehaviour
{
    void Start()
    {
        foreach (Transform child in transform)
            StartCoroutine (UpdateCanvas(child));
    }

    IEnumerator UpdateCanvas(Transform item)
	{
        yield return new WaitForEndOfFrame ();
        item.gameObject.SetActive (false);
		yield return new WaitForEndOfFrame();
        item.gameObject.SetActive (true);
    }

}
