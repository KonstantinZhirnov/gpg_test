﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelView : MonoBehaviour
{
    [SerializeField]
    Text _levelText;
    [SerializeField]
    Slider _levelProgress;

    UserLevel _data;

    private void Start ()
    {
        _data = GameManager.Instance.CurrentUser.Level;
        _data.OnLevelChanged += _data_OnLevelChanged;
    }

    //public UserLevel Data {
    //    get => _data;
    //    set {
    //        _data = value;
    //        _data.OnLevelChanged += _data_OnLevelChanged;
            
    //    }
    //}

    private void _data_OnLevelChanged (UserLevel obj)
    {
        _levelText.text = _data.Value.ToString ();
        _levelProgress.maxValue = _data.MaxXP;
        _levelProgress.value = _data.CurrentXP;
    }
}
