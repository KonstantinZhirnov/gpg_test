﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChiefView : CharacterView
{
    [System.Serializable]
    public class ChiefMenuItem {
        public PizzaView pizzaItem;
        public float pizzaCost;
        public int pizzaPieces;
    }
    public RotateTable table;

    [SerializeField]
    List<ChiefMenuItem> _menu;

    protected override void Start ()
    {
        OnActionCall += CharacterAction;
        base.Start ();
    }

    protected void CharacterAction ()
    {
        ChiefMenuItem selectedMenu = _menu [0];
        OnActionCall -= CharacterAction;
        StartCoroutine (AddPizza (selectedMenu));
        
    }

    IEnumerator AddPizza (ChiefMenuItem item) {
        while(!table.AddPizza (item, _actionPoint)) {
            yield return new WaitForEndOfFrame ();
        }
        OnActionCall += CharacterAction;
    }
}
