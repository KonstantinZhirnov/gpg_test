﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Collections.Generic;

public class CustomerView : CharacterView
{
    private event Action<string> OnAnimationFinish;

    [SerializeField]
    RotateTable _Table;
    [SerializeField]
    Transform _pizzaPlace;
    [SerializeField]
    Image _pieceIcon;

    [Header ("speed Settings")]
    [SerializeField]
    float _getPizzaSpeed = 10;
    [SerializeField]
    float _eatSpeed = 1;



    Animation _animation;
    PizzaView pizza = null;
    Animator _animator;

    protected override void Start ()
    {
        _animator = GetComponent<Animator> ();
        base.Start ();
        _animation = GetComponent<Animation> ();

        GetPizza ();
    }

    public void Eat ()
    {
        if (pizza != null) {
            pizza.Pizza.EatPiece ();
        }
    }

    protected void GetPizza ()
    {
        OnAnimationFinish -= AddPizzaEatenHandler;
        StartCoroutine (GetPizzaRoutine ());
    }

    IEnumerator GetPizzaRoutine ()
    {
        while (pizza == null) {
            pizza = _Table.GetPizza (_actionPoint);
            yield return null;
        }

        PlayAnimation ("CustomerGetPizza");
        TakePizza ();
    }

    private void TakePizza()
    {
        pizza.ClearCallbacks ();
        LeanTween.cancel (pizza.gameObject);
        pizza.isRotate = false;
        pizza.Speed = 3;
        pizza.Next = _pizzaPlace;
        _pieceIcon.sprite = pizza.PieceSprite;
    }

    private void PlacePizza ()
    {
        pizza.transform.SetParent (_pizzaPlace, false);
        pizza.transform.localPosition = Vector3.zero;
        OnAnimationFinish += AddEatHandler;
        PlayEatAnimation ();

        pizza.OnPizzaEaten += OnPizzaEatenHandler;
    }

    void PlayEatAnimation ()
    {
        Eat ();
        PlayAnimation ("CustomerEat");
    }

    void AddEatHandler (string animationName) {
        if(animationName == "CustomerEat") {
            PlayEatAnimation ();
        }
    }

    private void OnPizzaEatenHandler (PizzaView obj)
    {
        OnAnimationFinish -= AddEatHandler;
        OnAnimationFinish += AddPizzaEatenHandler;
        PlayEatAnimation ();
    }

    void AddPizzaEatenHandler (string animationName) {
        if (animationName == "CustomerEat") {
            GetPizza ();
        }
    }

    private void PlayAnimation(string animationName)
    { 
        StartCoroutine (PlayAnimationRoutine (animationName));
    }

    IEnumerator PlayAnimationRoutine(string animationName)
    {
        yield return new WaitForEndOfFrame ();
        _animator.Play (animationName);
        yield return new WaitForEndOfFrame ();
        while (_animator.GetCurrentAnimatorStateInfo (0).IsName (animationName)) {
            yield return new WaitForEndOfFrame ();
        }
        OnAnimationFinish?.Invoke (animationName);
    }
}
