﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class RotateTable : MonoBehaviour
{
    public event Action<float> OnSpeedChange;

    [SerializeField]
    Transform [] _movePoints;
    [SerializeField]
    [Range(1f, 19f)]
    float _moveSpeed;
    [SerializeField]
    int _takenItems = 2;

    [SerializeField]
    Transform _chevronsContainer;
    [SerializeField]
    TableItemView _chevron;

    [SerializeField]
    Transform _pizzaContainer;

    float _oldSpeed;

    public float MoveSpeed
    {
        get
        {
            return _moveSpeed;
        }

        set {
            _moveSpeed = value;
            OnSpeedChange?.Invoke (_moveSpeed);
        }
    }

    private List<PizzaView> _pizzasAvailable = new List<PizzaView> ();

    private void Start ()
    {
        _oldSpeed = _moveSpeed;
        CreateChevrons ();
    }

    private void Update ()
    {
        if (_oldSpeed != MoveSpeed) {
            MoveSpeed = _moveSpeed;
            _oldSpeed = MoveSpeed;
        }
    }

    private void CreateChevrons () {
        for (int i = 0; i < _movePoints.Length; i++) {
            TableItemView chevron = Instantiate (_chevron, _chevronsContainer);
            chevron.transform.localPosition = _movePoints [i].localPosition;
            chevron.transform.rotation = _movePoints [i].rotation;
            chevron.Init (this);
            int next = i;
            if (next >= _movePoints.Length - 1) {
                next = -1;
            }
            next++;

            chevron.Next = _movePoints [next];

            chevron.OnMoveFinish += OnItemMoveFinishHandler;
            chevron.gameObject.SetActive (true);
        }
    }

    private void OnItemMoveFinishHandler (TableItemView obj)
    {
        obj.Next = GetNextPoint (obj.Next);
    }

    public Transform GetNextPoint(Transform current)
    {
        int currentIndex = Array.IndexOf (_movePoints, current);
        if (currentIndex == _movePoints.Length - 1) {
            currentIndex = -1;
        }

        return _movePoints [++currentIndex];
    }

    public Transform GetPrevPoint (Transform current)
    {
        int currentIndex = Array.IndexOf (_movePoints, current);
        if (currentIndex == 0) {
            currentIndex = _movePoints.Length;
        }

        return _movePoints [--currentIndex];
    }

    public bool AddPizza (ChiefView.ChiefMenuItem menuItem, Transform point)
    {
        if (!isPlaceAvailable(point)) return false;

        PizzaView pizza = Instantiate (menuItem.pizzaItem, _pizzaContainer);
        pizza.Init (new Pizza (menuItem.pizzaPieces, menuItem.pizzaCost));

        pizza.transform.position = point.position;
        pizza.transform.rotation = point.rotation;
        pizza.transform.localScale = Vector3.one;
        (pizza as TableItemView).Init (this);
        pizza.Next = GetNextPoint(point);
        pizza.OnPizzaEaten += OnPizzaEatenHandler;
        pizza.OnMoveFinish += OnItemMoveFinishHandler;
        _pizzasAvailable.Add (pizza);

        return true;
    }

    bool isPlaceAvailable (Transform place)
    {
        PizzaView isPresent = null;
        Transform checkedPoint = place;

        isPresent = _pizzasAvailable.Find (item => item.Next == checkedPoint);
        if (isPresent != null) {
            return false;
        }

        for (int i = 0; i <= _takenItems; i++) {
            checkedPoint = GetNextPoint (checkedPoint);
            isPresent = _pizzasAvailable.Find (item => item.Next == checkedPoint);
            if(isPresent != null) {
                return false;
            }
        }

        checkedPoint = place;
        for (int i = _takenItems; i >= 0; i--) {
            checkedPoint = GetPrevPoint (checkedPoint);
            isPresent = _pizzasAvailable.Find (item => item.Next == checkedPoint);
            if (isPresent != null) {
                return false;
            }
        }


        return true;
    }

    public PizzaView GetPizza(Transform place)
    {
        return _pizzasAvailable.Find (item => item.Next == place);
    }

    private void OnPizzaEatenHandler (PizzaView obj)
    {
        _pizzasAvailable.Remove (obj);
        Destroy (obj.gameObject);
    }


}
