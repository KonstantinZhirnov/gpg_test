﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableItemView: MonoBehaviour
{
    public event Action<TableItemView> OnMoveFinish;

    public bool isRotate = true;

	Transform _next;
    RotateTable _parent;
    float _speed;

    LTDescr move;
    LTDescr rotate;

    Coroutine _resetRoutine;

    public Transform Next {
        get => _next;
        set {
            _next = value;
            createTweens ();

            //if (gameObject.activeSelf) {
            //    StartCoroutine (SetNextTween ());
            //}
        }
    }

    public float Speed {
        get => _speed;
        set => _speed = value;
    }

    public void Init(RotateTable parent)
    {
        _parent = parent;
        _parent.OnSpeedChange += _parent_OnSpeedChange;
        Speed = _parent.MoveSpeed;
    }

    public void ClearCallbacks ()
    {
        OnMoveFinish = null;
    }


    private void _parent_OnSpeedChange (float obj)
    {
        Speed = _parent.MoveSpeed;
        //if (gameObject.activeSelf && _resetRoutine == null) {
        //    _resetRoutine = StartCoroutine (SetNextTween ());
        //}
    }

    protected IEnumerator SetNextTween ()
    {
        LeanTween.cancel (gameObject);
        yield return new WaitForEndOfFrame ();
        createTweens ();
        _resetRoutine = null;
    }

    private void createTweens ()
    {
        Vector3 source = transform.position;
        Vector3 destination = Next.position;

        Vector3 difference = source - destination;

        move = LeanTween.move (gameObject, destination, 1f / Speed).setOnComplete (val => { OnMoveFinish?.Invoke(this); });
        if (isRotate) {
            rotate = LeanTween.rotate (gameObject, new Vector3 (0f, 0f, Next.rotation.eulerAngles.z), 1f / Speed);
        }
    }
}
