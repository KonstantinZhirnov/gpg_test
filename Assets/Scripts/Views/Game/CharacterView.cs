﻿using System.Collections;
using System;
using UnityEngine;

public class CharacterView : MonoBehaviour
{
    public event Action OnActionCall;

    [SerializeField]
    protected Transform _actionPoint;
    [SerializeField]
    private float _actionDelay;

    public bool isLoop = true;

    private Coroutine _routine;

    protected float ActionDelay {
        get => _actionDelay;
        set {
            _actionDelay = value;
            StartCoroutine (Reset ());
        }
    }

    protected virtual void Start ()
    {
        _routine = StartCoroutine (ActionCoroutine ());
    }

    IEnumerator ActionCoroutine ()
    {
        OnActionCall?.Invoke ();
        while (isLoop) {
            yield return new WaitForSeconds (ActionDelay);
            OnActionCall?.Invoke ();
        }
    }

    protected void Stop ()
    {
        StopCoroutine (_routine);
    }

    IEnumerator Reset()
    {
        Stop ();
        yield return new WaitForEndOfFrame ();
        _routine = StartCoroutine (ActionCoroutine ());
    }
}
