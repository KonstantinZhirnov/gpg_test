﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PizzaView : TableItemView
{
    public event Action<PizzaView> OnPizzaEaten;

    [Header ("Object settings")]
    [SerializeField]
    Image _pizzaIcon;

    [Header("View settings")]
    [SerializeField]
    Sprite _pieceSprite;

    public Sprite PieceSprite
    {
        get => _pieceSprite;
    }

    Pizza _pizza;

    public Pizza Pizza
    {
        get => _pizza;
    }

    public void Init (Pizza content)
    {
        _pizza = content;
        _pizza.OnPiaceEaten += _pizza_OnPiaceEaten;
        _pizza.OnPizzaEaten += _pizza_OnPizzaEaten;
    }

    private void _pizza_OnPizzaEaten (Pizza obj)
    {
        OnPizzaEaten?.Invoke (this);
    }

    private void _pizza_OnPiaceEaten (Pizza obj)
    {
        GameManager.Instance.CurrentUser.Coins += obj.PiecePrise;
        GameManager.Instance.CurrentUser.Level.CurrentXP += obj.PiecePrise;
        _pizzaIcon.fillAmount = (float)obj.PiecesLeft / obj.PiecesCount;
    }
}
