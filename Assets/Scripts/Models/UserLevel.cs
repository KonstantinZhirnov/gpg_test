﻿using System;

public class UserLevel
{
    public event Action<UserLevel> OnLevelChanged;

    private int _value;
    private float _currentXP;
    private float _maxXP = 50f;

    public int Value
    {
        get => _value;
        set => this._value = value;
    }

    public float CurrentXP
    {
        get => _currentXP;
        set
        {
            _currentXP = value;
            if(_currentXP > _maxXP) {
                _currentXP -= _maxXP;
                _maxXP *= 2;
                _value++;
            }

            OnLevelChanged?.Invoke (this);
        }
    }

    public float MaxXP
    {
        get => _maxXP;
        set => _maxXP = value;
    }


}
