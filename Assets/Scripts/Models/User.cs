﻿
using System;
[Serializable]
public class User
{
    public event Action<float> OnCoinsChanged;

    
    UserLevel _level = new UserLevel();
    
    float _coins = 0f;

    public UserLevel Level
    {
        get => _level;
        set => _level = value;
    }

    public float Coins
    {
        get => _coins;
        set
        {
            _coins = value;
            OnCoinsChanged?.Invoke (_coins);
        }
    }
}
