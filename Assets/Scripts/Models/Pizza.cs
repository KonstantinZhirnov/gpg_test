﻿using System;

public class Pizza 
{
    public event Action<Pizza> OnPiaceEaten;
    public event Action<Pizza> OnPizzaEaten;

    int _piecesCount;
    float _piecePrise;

    int _piecesLeft;

    public float PiecePrise
    {
        get
        {
            return _piecePrise;
        }
    }

    public int PiecesCount {
        get {
            return _piecesCount;
        }
    }

    public int PiecesLeft {
        get {
            return _piecesLeft;
        }
    }

    public Pizza (int piecesCount, float piecePrise)
    {
        _piecePrise = piecePrise;
        _piecesCount = piecesCount;
        _piecesLeft = _piecesCount;
    }

    public void EatPiece()
    {
        if (_piecesLeft <= 0) return;

        _piecesLeft--;
        OnPiaceEaten?.Invoke (this);

        if(_piecesLeft <= 0) {
            OnPizzaEaten?.Invoke (this);
        }
    }

}
